<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage faculty</title>
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

</head>
<body>
<?php
SESSION_START();
if($_SESSION['xy'])
{
	$get=$_SESSION['xy'];
}
else
{
	header("location:adm_log.php");
}
?>
<div id="tooplate_header">

    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
              <li><a href="ch_adm.php" ><?php echo $get ?></a></li>


            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>
     <div id="tooplate_mid_wrapper">
    	<div id="tooplate_mid_home">



            <div id="mid_left">
                <div id="mid_title">
                   <font color="#000000"> Faculty Manage</font>
                </div>
                <p id="mid_text"> <font color="#FF6600"> Choose from below options to manage the  faculty information.</font></p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>

        </div>
    </div>
</div>




 <?php
			  if(isset($_POST['b']))
			  {
				  SESSION_DESTROY();
				  header("location:index.php");
			  }
			  ?>





<div id="tooplate_main">
	<div class="col_3">

        <div class="cleaner h10"></div>
         <form action="" method="post">
         <a href="reg_faculty.php"><img src="images/Register-Button.png" height="60" width="250" /></a></form>
    </div>
    <div class="col_3">

        <div class="cleaner h10"></div>
         <form action="" method="post">
       <a href="update_fac.php"><img src="images/update.png" height="60" width="250" /></a></form>
    </div>
    <div class="col_3 col_l">

        <div class="cleaner h10"></div>
         <form action="" method="post">
        <a href="del_fac.php"><img src="images/delete.png" height="60" width="250" /></a></form>

        <div class="cleaner h10"></div>
        <p> </p>
    </div>

    <div class="cleaner"></div>
</div>

<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>


</body>
</html>
