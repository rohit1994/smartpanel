<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BKBIET- Contact</title>
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

</head>
<body>
<div id="tooplate_header">

    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
              <li><a href="index.php" >Home</a></li>

              <li><a href="contact.php" class="selected">Contact</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>


</div> <!-- end of header -->






<div id="tooplate_main">
	<div class="col_2 float_l">
    	<h4>Quick Contact Form</h4>
        <div id="contact_form">
            <form method="post" name="contact" action="#">

				<label for="author">Name:</label> <input type="text" id="author" name="author" class="required input_field" required/>
				<div class="cleaner h10"></div>

				<label for="email">Email:</label> <input type="text" class="validate-email required input_field" name="email" id="email" required/>
				<div class="cleaner h10"></div>

				<label for="subject">Subject:</label> <input type="text" class="validate-subject required input_field" name="subject" id="subject" required/>
				<div class="cleaner h10"></div>

				<label for="text">Message:</label> <textarea id="text" name="text" rows="0" cols="0" class="required" required></textarea>
				<div class="cleaner h10"></div>

				<input type="submit" value="Submit" id="submit" name="submit" class="submit_btn float_l" />
				<input type="reset" value="Reset" id="reset" name="reset" class="submit_btn float_r" />

            </form>
            <?php 
			include("db.php");
			if(isset($_POST['submit']))
			{
				$name=$_POST['author'];
				$email=$_POST['email'];
				$sub=$_POST['subject'];
				$msg=$_POST['text'];
				mysql_query("insert into contact values('','$name','$email','$sub','$msg')");
			}
			?>
				
        </div>
		
    </div>
    <div class="col_2 float_r">
    	<h4>Our Location</h4>
        <div class="image_frame">
        <a href="images/map.jpg" rel="lightbox" title="Our Location"><img src="images/map.jpg" alt="Our Location" height="200" width="400" /></a></div>
        <div class="cleaner h20"></div>
            <a href="https://www.google.com/maps/place/BKBIET/@28.0501671,75.6271366,8z/data=!4m2!3m1!1s0x39131bd8fa3e1421:0xd7928a8c86ca1d5e?hl=en" target="_blank">click!  </a>To see the location on google Map. </br></br>

            <h3>Mailing Address</h3>
            <h5>BKBIET</h5>
            BKBIET Campus, <br />
            CEERI Road, Pilani - 333031,<br />
            Dist. Jhunjhunu, Rajasthan, INDIA<br /><br />
            <strong>Phone:</strong> 01596-246092, 9414062512<br />
            <strong>Email:</strong> <a href="mailto:dean.csit@bkbiet.ac.in">dean.csit@bkbiet.ac.in</a> / <a href="mailto:dean.csit@bkbiet.ac.in">dean.csit@bkbiet.ac.in</a>
    </div>
    <div class="cleaner"></div>
</div>

<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>

</body>
</html>
