<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin | Delete Time Table</title>
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

</head>
<body>
<?php
SESSION_START();
if($_SESSION['xy'])
{
	$get=$_SESSION['xy'];
}
else
{
	header("location:adm_log.php");
}
?>
<div id="tooplate_header">

    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
             <li><a href="adm_wel.php" ><?php echo $get ?></a></li>
               <li><a href="adm_tt.php" class="selected" >Time Table</a></li>
                <li><a href="adm_nt.php" >Notification</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>


</div> <!-- end of header -->

  <div id="tooplate_mid_wrapper">
    	<div id="tooplate_mid_home">



            <div id="mid_left">
                <div id="mid_title">
                   <font color="#000000"> Delete student details</font>
                </div>
                <p id="mid_text"> <font color="#FF6600"> Enter id of the Time Table below to delete details permanently. You can get the id from the "show time table" link on previous page.</font></p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>

        </div>
    </div>
</div>







<div id="tooplate_main">
<div class="col_2 float_r">
<h5>      <?php
include("db.php");

if(isset($_POST['del_s']))
{
// username and password sent from Form
$ttid=$_POST['ttid'];



$sql="delete from time_table where id='$ttid'";
$result=mysql_query($sql);
echo "Thank you! $ttid has deleted .<br/> Fill the form again to delete another Time table!!! ";
}

 ?>  </h5> </div>
	<div class="col_2 float_l">

    	<h4>Provide info to delete the Time table!!</h4>
        <div id="contact_form">


            <form method="post" name="contact" action="#">

            <label for="">Id:</label> <input type="text" name="ttid"  class="required input_field" required />
				<div class="cleaner h10"></div>



				<input type="submit" value="Delete" id="submit" name="del_s" class="submit_btn float_l" />
				<input type="reset" value="Reset" id="reset" name="reset" class="submit_btn float_r" />

            </form>





        </div>
    </div>

    <div class="cleaner"></div>
</div>

<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>

</body>
</html>
