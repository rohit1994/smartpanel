<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BKBIET Smart Panel</title>
<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />


<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />


</head>
<body>

<div id="tooplate_header">
	
    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
              <li><a href="index.php" class="selected">Home</a></li>
          		
              <li><a href="contact.php">Contact</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>
    
    <div id="tooplate_mid_wrapper">
    	<div id="tooplate_mid_home">
        	
           

            <div id="mid_left">
                <div id="mid_title">
                   <font color="#000000"> Introducing BKBIET Smart Panel</font>
                </div>
                <p id="mid_text"> <font color="#FF6600"> Smart panel is designed to make a Bridge between student & Faculty and to make their work more comfortable and easy.</font></p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>
        	
        </div>
    </div>
</div> <!-- end of header -->

<div id="tooplate_main">
	<div class="col_3">
    	<div class="title_with_icon"><img src="images/presentation.png" alt="image" /><h3>Student Login</h3></div>
        <div class="cleaner h10"></div>
        <p>Registered student can login here to check their attendence, time table,notifications and submit assignments.<br/><br/><a href="st_log.php"><img src="images/button-student-login.png" height="50" width="150" /></a></p>
    </div>
    <div class="col_3">
    	<div class="title_with_icon"><img src="images/preferences.png" alt="image" /><h3>Faculty Login</h3></div>
        <div class="cleaner h10"></div>
        <p>Respective faculty can login here to store the attendence of related branch students,display notification, time table and collect assignment.<br/><br/><a href="fac_log.php"><img src="images/Faculty.png" height="50" width="150" /></a></p>
    </div>
    <div class="col_3 col_l">
    	<div class="title_with_icon"><img src="images/magic_wand.png" alt="image" /><h3>Admin Login<h3></div>
        <div class="cleaner h10"></div>
        <P> Admin login to access the accounts of students and faculty and maintain thier privilages.</P> <br/><br/><a href="adm_log.php"><img src="images/admin-login.png" height="50" width="150" /></a>
        <div class="cleaner h10"></div>
        <p> </p>
    </div>
   
    <div class="cleaner"></div>
</div>

 
<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>

</body>
</html>