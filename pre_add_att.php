<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
SESSION_START();
if($_SESSION['fac'])
{
	$get=$_SESSION['fac'];
}
else
{
	header("location:fac_log.php");
}
?>
<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Smart Panel | Add Attendance</title>
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

</head>
<body>
<div id="tooplate_header">

    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
               <li><a href="fac_wel.php" >Mr.<?php echo $get ?></a></li>
              <li><a href="not_fac.php" >Notifications</a></li>
              <li><a href="deb_fac.php" >Debarred List</a></li>
              <li><a href="deb_fac.php" >Assignment</a></li>
              <li><a href="for_att.php"  class="selected">Attendance</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>


<div id="tooplate_mid_wrapper">
    	<div id="tooplate_mid_home">



            <div id="mid_left">
                <div id="mid_title">
                   <font color="#000000"> Welcome <?php echo $get; ?> </font>
                </div>
                <p id="mid_text"> <font color="#FF6600"> enter the branch and year to add attendance for toaday.Please fill the details in small letters do not use numbers.</font></p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>

        </div>
    </div>
</div>






<div id="tooplate_main">
	<div class="col_2 float_l">
    	<h4>Fill details</h4>
        <div id="contact_form">
            <form method="post"  action="#" enctype="multipart/form-data">
				<div class="cleaner h10"></div>
				<label for="branch">Branch:</label> <select class="validate-email required input_field" name="branch" id="branch" placeholder="assignment for:" required><option value="cs">CS</option>
                                        <option value="ee">EE</option>
                                        <option value="eee">EEE</option>
                                        <option value="ec">EC</option>
                                        <option value="it">IT</option>
                                        </select>
				<div class="cleaner h10"></div>
				<label for="year">year:</label> <select class="validate-email required input_field" name="year" id="year" placeholder="year " required>
                <option value="first">First</option>
                                        <option value="second">Second</option>
                                        <option value="third">Third</option>
                                        <option value="fourth">Fourth</option>
                                        </select>
				<div class="cleaner h10"></div>
                <label for="subject">subject:</label> <input type="text" class="validate-email required input_field"  name="sub"  placeholder="subject " required/>
				<div class="cleaner h10"></div>



				<input type="submit" value="Submit" id="submit" name="att" class="submit_btn float_l"  />
				<input type="reset" value="Reset" id="reset" name="reset" class="submit_btn float_r" />

            </form>
            
            
            
            
            <?php
include("db.php");
if(isset($_POST['att']))
{

	$branch=$_POST['branch'];
	$year=$_POST['year'];
	$sub=$_POST['sub'];

		$_SESSION['branch']=$branch;
		$_SESSION['subject']=$sub;
		$_SESSION['year']=$year;
		header("location:add_att.php");
		}
?>
      
  
        </div>
    </div>

    <div class="cleaner"></div>
</div>



<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>

</body>
</html>
