-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 14, 2015 at 05:58 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `proj`
--
CREATE DATABASE IF NOT EXISTS `proj` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `proj`;

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `Name`, `Username`, `Password`) VALUES
(1, 'xyz', 'abc', 'abc');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE IF NOT EXISTS `assignment` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date` varchar(20) NOT NULL,
  `branch` varchar(20) NOT NULL,
  `year` varchar(20) NOT NULL,
  `sdate` varchar(20) NOT NULL,
  `topic` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `name`, `date`, `branch`, `year`, `sdate`, `topic`, `description`) VALUES
(1, 'gopal', '08/07/2015', 'cs', 'first', '10/07/2015', 'testing', 'jhsvdhvashkdvkasvdasvjfvashkfv\r\nsfhjasfjlfbljkas'),
(3, 'gopal', '09/07/2015', 'cs', 'first', '10/07/2015', 'bkjdvkjv', 'svadcshadkhask');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
  `roll` varchar(20) NOT NULL,
  `present` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `branch` varchar(20) NOT NULL,
  `year` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`roll`, `present`, `date`, `subject`, `branch`, `year`) VALUES
('12ebkcs091', 'Yes', '2015/07/10', 'english', 'cs', 'first'),
('12ebkcs108', 'Yes', '2015/07/10', 'english', 'cs', 'first'),
('12ebkcs111', 'Yes', '2015/07/10', 'english', 'cs', 'first'),
('12ebkcs091', 'No', '2015/07/12', 'hindi', 'cs', 'first'),
('12ebkcs108', 'Yes', '2015/07/12', 'hindi', 'cs', 'first'),
('12ebkcs111', 'Yes', '2015/07/12', 'hindi', 'cs', 'first');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`) VALUES
(1, '', 'abc@gmail.com', 'test', ''),
(2, 'Rohit yadav', 'abc@gmail.com', 'test', ''),
(3, 'Rohit yadav', 'abc@gmail.com', 'test', 'tseting\r\ntest');

-- --------------------------------------------------------

--
-- Table structure for table `debarred`
--

CREATE TABLE IF NOT EXISTS `debarred` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date` varchar(10) NOT NULL,
  `branch` varchar(10) NOT NULL,
  `year` varchar(10) NOT NULL,
  `upd` varchar(1000) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `debarred`
--

INSERT INTO `debarred` (`id`, `name`, `date`, `branch`, `year`, `upd`, `comment`) VALUES
(1, 'dsa', '0000-00-00', 'sad', 'sadd', 'file_upload/11949847621854578794pill-button-blue_benji_p_01.svg.hi - Copy (2).png', 'sdvjavdhvsha'),
(2, 'sdvjvj', '2015-07-01', 'jhsvdhj', 'jdcsjhca', 'file_upload/aa.jpg', 'shdvasv'),
(3, 'dvsa', '2015-09-12', 'sdvj', 'hsdva', 'file_upload/expences-button-png-hi - Copy.png', 'hsdas');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `faculty_id` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`faculty_id`, `first_name`, `last_name`, `gender`, `email`, `contact_no`, `password`) VALUES
('1', 'gopal', 'prajapat', 'male', 'gopal@gmail.com', '8965321245', 'gopal'),
('2', 'jitendra ', 'sharma', 'male', 'jitu@gmail.com', '6986325569', 'jitu');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date` varchar(10) NOT NULL,
  `branch` varchar(10) NOT NULL,
  `year` varchar(10) NOT NULL,
  `topic` varchar(100) NOT NULL,
  `upd` varchar(1000) DEFAULT 'file_upload/expences-button-png-hi.png',
  `descriptiion` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `name`, `date`, `branch`, `year`, `topic`, `upd`, `descriptiion`) VALUES
(1, 'gopal', '24/08/1994', 'cs', 'first', 'testing', 'file_upload/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png', 'sdadasdasd'),
(2, 'ajay', '24/8/1954', 'ee', 'first', 'pata nahi', 'file_upload/expences-button-png-hi.png', 'sadasdasf\r\nfasklbafjbafs\r\nnfakbak;f\r\nbfaslkbsk\r\nnafslkbask\r\n'),
(3, 'vivek', '24/8/111', 'ee', 'seconf', 'sdljasvd', 'file_upload/faculty-login.png', 'sdasfd'),
(4, 'vvv', '24/88/999', 'cs', 'first', 'dasdsad', 'file_upload/11949847621854578794pill-button-blue_benji_p_01.svg.hi - Copy (2).png', 'sdasfaksfjkvsajkf\r\nashfjlsvfkjvfsa\r\nbflabfljksa\r\ngfkgasljf\r\ngalsfglsa\r\ngfasljkbslkf\r\nbljfblas\r\nvaskjfvalsjf\r\nvjkvfjlsa\r\nbalsfbjlsab\r\nbafljbaslkjf\r\nbsfajkbalk;sf\r\n'),
(5, 'ww', '24/09/2015', 'cs', 'second', 'testing', 'file_upload/jquery-1.11.3.min.js', 'svfkjvaskfvkjvsfakvfkavfs');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `roll` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `Father_name` varchar(50) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `branch` varchar(20) NOT NULL,
  `year` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`roll`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`roll`, `first_name`, `last_name`, `Father_name`, `mail`, `contact_no`, `branch`, `year`, `password`) VALUES
('12ebkcs091', 'Rohit', 'Yadav', 'Ramesh Yadav', 'rohit.yadav@hotmail.co.in', '7737889813', 'cs', 'first', '2424'),
('12ebkcs108', 'Surabhi', 'Shekhawat', 'Pratap singh', 's.shekhawat94@gmail.com', '9772235446', 'cs', 'first', '2408'),
('12ebkcs111', 'tikam', 'singh', 'patanahi', 'patanahi@gmail.com', '7788996633', 'cs', 'first', 'singhada'),
('12ebkee072', 'pushp', 'rathore', 'gunman singh', 'pushpa@gmail.com', '8387864481', 'ee', 'first', 'pushpa'),
('12ebkex018', 'gaurav', 'yadav', 'pata nahi', 'gauarav@gmail.com', '9865321245', 'eee', 'first', '2222');

-- --------------------------------------------------------

--
-- Table structure for table `subass`
--

CREATE TABLE IF NOT EXISTS `subass` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `roll` varchar(20) NOT NULL,
  `branch` varchar(20) NOT NULL,
  `year` varchar(20) NOT NULL,
  `faculty` varchar(50) NOT NULL,
  `Topic` varchar(50) NOT NULL,
  `date` varchar(20) NOT NULL,
  `ass` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `subass`
--

INSERT INTO `subass` (`id`, `roll`, `branch`, `year`, `faculty`, `Topic`, `date`, `ass`) VALUES
(1, '12ebkcs091', 'cs', 'first', 'gopal', 'eqwewqe', '09/07/2015', ''),
(2, '12ebkcs091', 'cs', 'first', 'gopal', 'nsbdhkasvghd', '09/07/2015', 'file_upload/css_tutorial.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `time_table`
--

CREATE TABLE IF NOT EXISTS `time_table` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `branch` varchar(10) NOT NULL,
  `year` varchar(10) NOT NULL,
  `upd` varchar(1000) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `time_table`
--

INSERT INTO `time_table` (`id`, `date`, `branch`, `year`, `upd`, `comment`) VALUES
(1, '0000-00-00', 'sad', 'sadd', 'file_upload/11949847621854578794pill-button-blue_benji_p_01.svg.hi - Copy (2).png', 'sdvjavdhvsha'),
(2, '2015-07-01', 'jhsvdhj', 'jdcsjhca', 'file_upload/aa.jpg', 'shdvasv'),
(3, '2015-09-12', 'sdvj', 'hsdva', 'file_upload/expences-button-png-hi - Copy.png', 'hsdas');

-- --------------------------------------------------------

--
-- Table structure for table `virtual`
--

CREATE TABLE IF NOT EXISTS `virtual` (
  `a` varchar(100) NOT NULL,
  `b` varchar(100) NOT NULL,
  `c` varchar(100) NOT NULL,
  `d` varchar(100) NOT NULL,
  `e` varchar(100) NOT NULL,
  `f` varchar(100) NOT NULL,
  `g` varchar(100) NOT NULL,
  `h` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
