<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Register student</title>
<?php
SESSION_START();
if($_SESSION['xy'])
{
	$get=$_SESSION['xy'];
}
else
{
	header("location:adm_log.php");
}
?>
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

</head>
<body>

<div id="tooplate_header">

    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
              <li><a href="ch_adm.php" ><?php echo $get ?></a></li>

              <li><a href="adm_wel.php" >Home</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>


</div> <!-- end of header -->

  <div id="tooplate_mid_wrapper">
    	<div id="tooplate_mid_home">



            <div id="mid_left">
                <div id="mid_title">
                   <font color="#000000"> Introducing BKBIET Smart Panel</font>
                </div>
                <p id="mid_text"> <font color="#FF6600"> Smart panel is designed to make a Bridge between student & Faculty and to make their work more comfortable and easy.</font></p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>

        </div>
    </div>
</div>







<div id="tooplate_main">
<div class="col_2 float_r">
<h5>      <?php
include("db.php");

if(isset($_POST['reg_s']))
{
// username and password sent from Form
$roll=$_POST['urn'];
$first=$_POST['sfn'];
$last=$_POST['sln'];
$papa=$_POST['sffn'];
$email=$_POST['email'];
$contact=$_POST['cn'];
$branch=$_POST['bran'];
$year=$_POST['yea'];
$pass=$_POST['scpsw'];



$sql="insert into student values('$roll','$first','$last','$papa','$email','$contact','$branch','$year','$pass')";

$result=mysql_query($sql);
echo "Thank you!  $roll is registered.<br/> Fill the form again to register another student!!! ";
}

 ?>  </h5> </div>
	<div class="col_2 float_l">

    	<h4>Provide info to register the student!!</h4>
        <div id="contact_form">


            <form method="post" name="contact" action="#">

            <label for="">University roll no:</label> <input type="text" name="urn"  class="required input_field" required />
				<div class="cleaner h10"></div>

				<label for="">First Name:</label> <input type="text" name="sfn"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="">Last Name:</label> <input type="text" name="sln"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="">Father's Name:</label> <input type="text" name="sffn"    class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="email">Email:</label> <input type="email" class="validate-email required input_field" name="email" id="email" required/>
				<div class="cleaner h10"></div>
                <label for="">contact no.:</label> <input type="text" name="cn"   class="required input_field" required />
				<div class="cleaner h10"></div>
               <label for="branch">Branch:</label> <select class="validate-email required input_field" name="bran" id="branch" placeholder="assignment for:" required><option value="cs">CS</option>
                                        <option value="ee">EE</option>
                                        <option value="eee">EEE</option>
                                        <option value="ec">EC</option>
                                        <option value="it">IT</option>
                                        </select>
				<div class="cleaner h10"></div>
				<label for="year">year:</label> <select class="validate-email required input_field" name="yea" id="year" placeholder="year " required>
                <option value="first">First</option>
                                        <option value="second">Second</option>
                                        <option value="third">Third</option>
                                        <option value="fourth">Fourth</option>
                                        </select>
				<div class="cleaner h10"></div>


				 <label for="">Create Password:</label> <input type="password" name="scpsw"   class="required input_field" required />
				<div class="cleaner h10"></div>

				<input type="submit" value="Register" id="submit" name="reg_s" class="submit_btn float_l" />
				<input type="reset" value="Reset" id="reset" name="reset" class="submit_btn float_r" />

            </form>





        </div>
    </div>

    <div class="cleaner"></div>
</div>

<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>

</body>
</html>
