<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Register student</title>
<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

</head>
<body>
<?php
SESSION_START();
if($_SESSION['xy'])
{
	$get=$_SESSION['xy'];
}
else
{
	header("location:adm_log.php");
}
?>
<div id="tooplate_header">

    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
              <li><a href="ch_adm.php" ><?php echo $get ?></a></li>

              <li><a href="adm_wel.php" >Home</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>


</div> <!-- end of header -->

  <div id="tooplate_mid_wrapper">
    	<div id="tooplate_mid_home">



            <div id="mid_left">
                <div id="mid_title">
                   <font color="#000000"> Update faculty</font>
                </div>
                <p id="mid_text"> <font color="#FF6600"> Enter required details below to update details of registered faculty member in database.</font></p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>

        </div>
    </div>
</div>







<div id="tooplate_main">
<div class="col_2 float_r">
<h5>      <?php
include("db.php");

if(isset($_POST['up_f']))
{

$oid=$_POST['ofid'];
$id=$_POST['fid'];
$first=$_POST['ffn'];
$last=$_POST['fln'];
$gender=$_POST['fgender'];
$email=$_POST['femail'];
$contact=$_POST['fcn'];
$pass=$_POST['fpsw'];


$sql1="update faculty set first_name='$first' where faculty_id='$oid'";
$sql2="update faculty set last_name='$last' where faculty_id='$oid'";
$sql3="update faculty set gender='$gender' where faculty_id='$oid'";
$sql4="update faculty set email='$email' where faculty_id='$oid'";
$sql5="update faculty set contact_no='$contact' where faculty_id='$oid'";

$sql6="update faculty set password='$pass' where faculty_id='$oid'";



$sql7="update faculty set faculty_id='$id' where faculty_id='$oid'";
mysql_query($sql1);
mysql_query($sql2);
mysql_query($sql3);
mysql_query($sql4);
mysql_query($sql5);
mysql_query($sql6);
mysql_query($sql7);


echo "Thank you! $oid updated to $id.<br/> Fill the form again to update another student!!! ";
}

 ?>  </h5> </div>
	<div class="col_2 float_l">

    	<h4>Provide info to update student info!!</h4>
        <div id="contact_form">


            <form method="post" name="contact" action="#">

             <label for="">Old ID:</label> <input type="text" name="ofid"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <p>Enter new details</p>
            <label for="">New ID:</label> <input type="text" name="fid"  class="required input_field" required />
				<div class="cleaner h10"></div>

				<label for="">First Name:</label> <input type="text" name="ffn"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="">Last Name:</label> <input type="text" name="fln"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="">gender:</label> <input type="text" name="fgender"    class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="email">Email:</label> <input type="email" class="validate-email required input_field" name="femail" id="email" required/>
				<div class="cleaner h10"></div>
                <label for="">contact no.:</label> <input type="text" name="fcn"   class="required input_field" required />
				<div class="cleaner h10"></div>

				 <label for="">Create Password:</label> <input type="password" name="fpsw"   class="required input_field" required />
				<div class="cleaner h10"></div>


				<input type="submit" value="Update" id="submit" name="up_f" class="submit_btn float_l" />
				<input type="reset" value="Reset" id="reset" name="reset" class="submit_btn float_r" />

            </form>





        </div>
    </div>

    <div class="cleaner"></div>
</div>

<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>

</body>
</html>
