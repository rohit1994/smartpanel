<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update student</title>
<link rel="shortcut icon" href="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />



<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

</head>
<body>
<?php
SESSION_START();
if($_SESSION['xy'])
{
	$get=$_SESSION['xy'];
}
else
{
	header("location:adm_log.php");
}
?>
<div id="tooplate_header">

    <div id="tooplate_titlebar">
    	<div id="site_title" ><h1><a href="#"><img src="images/Logo_BK_Birla_Institute_of_Engineering_&_Technology_Pilani.png"   /></a></h1></div>
        <div id="site_title" class="bk"><font size="+3" id="bk">BKBIET<br/><br/> Smart Panel</font></div>
        <div id="tooplate_menu"  class="ddsmoothmenu" >
            <ul>
              <li><a href="ch_adm.php" ><?php echo $get ?></a></li>

              <li><a href="adm_wel.php" >Home</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of tooplate_menu -->
    </div>


</div> <!-- end of header -->

  <div id="tooplate_mid_wrapper">
    	<div id="tooplate_mid_home">



            <div id="mid_left">
                <div id="mid_title">
                   <font color="#000000"> Update student</font>
                </div>
                <p id="mid_text"> <font color="#FF6600"> Enter required details below to update details of registered student in database.</font></p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>

        </div>
    </div>
</div>







<div id="tooplate_main">
<div class="col_2 float_r">
<h5>      <?php
include("db.php");

if(isset($_POST['up_s']))
{
// username and password sent from Form
$oroll=$_POST['ourn'];
$roll=$_POST['urn'];
$first=$_POST['sfn'];
$last=$_POST['sln'];
$papa=$_POST['sffn'];
$email=$_POST['email'];
$contact=$_POST['cn'];
$branch=$_POST['bran'];
$year=$_POST['yea'];
$pass=$_POST['scpsw'];


$sql1="update student set first_name='$first' where roll='$oroll'";
$sql2="update student set last_name='$last' where roll='$oroll'";
$sql3="update student set father_name='$papa' where roll='$oroll'";
$sql4="update student set mail='$email' where roll='$oroll'";
$sql5="update student set contact_no='$contact' where roll='$oroll'";
$sql6="update student set branch='$branch' where roll='$oroll'";
$sql7="update student set year='$year' where roll='$oroll'";
$sql8="update student set password='$pass' where roll='$oroll'";



$sql9="update student set roll='$roll' where roll='$oroll'";
mysql_query($sql1);
mysql_query($sql2);
mysql_query($sql3);
mysql_query($sql4);
mysql_query($sql5);
mysql_query($sql6);
mysql_query($sql7);
mysql_query($sql8);
mysql_query($sql9);

echo "Thank you! $oroll updated to $roll.<br/> Fill the form again to update another student!!! ";
}

 ?>  </h5> </div>
	<div class="col_2 float_l">

    	<h4>Provide info to update student info!!</h4>
        <div id="contact_form">


            <form method="post" name="contact" action="#">

            <label for="">Old University roll no:</label> <input type="text" name="ourn"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <p>Enter new details</p>
            <label for="">New University roll no:</label> <input type="text" name="urn"  class="required input_field" required />
				<div class="cleaner h10"></div>

                <label for="">First Name:</label> <input type="text" name="sfn"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="">Last Name:</label> <input type="text" name="sln"  class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="">Father's Name:</label> <input type="text" name="sffn"    class="required input_field" required />
				<div class="cleaner h10"></div>
                <label for="email">Email:</label> <input type="email" class="validate-email required input_field" name="email" id="email" required/>
				<div class="cleaner h10"></div>
                <label for="">contact no.:</label> <input type="text" name="cn"   class="required input_field" required />
				<div class="cleaner h10"></div>
              <label for="branch">Branch:</label> <select class="validate-email required input_field" name="bran" id="branch" placeholder="assignment for:" required><option value="cs">CS</option>
                                        <option value="ee">EE</option>
                                        <option value="eee">EEE</option>
                                        <option value="ec">EC</option>
                                        <option value="it">IT</option>
                                        </select>
				<div class="cleaner h10"></div>
				<label for="year">year:</label> <select class="validate-email required input_field" name="yea" id="year" placeholder="year " required>
                <option value="first">First</option>
                                        <option value="second">Second</option>
                                        <option value="third">Third</option>
                                        <option value="fourth">Fourth</option>
                                        </select>
				<div class="cleaner h10"></div>



				 <label for="">Create Password:</label> <input type="password" name="scpsw"   class="required input_field" required />
				<div class="cleaner h10"></div>



				<input type="submit" value="Update" id="submit" name="up_s" class="submit_btn float_l" />
				<input type="reset" value="Reset" id="reset" name="reset" class="submit_btn float_r" />

            </form>





        </div>
    </div>

    <div class="cleaner"></div>
</div>

<div id="tooplate_cr_bar_wrapper">
	<div id="tooplate_cr_bar">
     <div class="footer_social_button">
                <a href="#"><img alt="Facebook" src="images/facebook-32x32.png" title="facebook" /></a>
                <a href="#"><img alt="Flickr" src="images/flickr-32x32.png" title="flickr" /></a>
                <a href="#"><img alt="Twitter" src="images/twitter-32x32.png" title="twitter" /></a>
                <a href="#"><img alt="Youtube" src="images/youtube-32x32.png" title="youtube" /></a>
                <a href="#"><img alt="RSS" src="images/rss-32x32.png" title="rss" /></a>
			</div>
	Copyright © 2015 Developed By: <a href="#">Rohit Yadav</a>
    </div>
</div>

</body>
</html>
